package SantiagoBrozt.Main;

import SantiagoBrozt.InterfazDeUsuario.InscribirParticipante;
import SantiagoBrozt.Modelo.SistemaDeConcursos;
import SantiagoBrozt.Persistencia.AccesoConcursosCSV;
import SantiagoBrozt.Persistencia.AccesoConcursosDB;
import SantiagoBrozt.Persistencia.AccesoParticipantesCSV;
import SantiagoBrozt.Persistencia.AccesoParticipantesDB;

import javax.swing.*;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {

    public static final String URL = "jdbc:mysql://localhost:3306/tp4_ejercicio3";
    public static final String USUARIO = "SantiagoBrozt";
    public static final String COTRASENIA = "okQFuK8ohJKeeeiK";

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                   new Main().start();
                } catch (Exception e) {
// log exception...
                    System.out.println(e.getMessage());
                }
            }
        });
    }
    private void start() {


        new InscribirParticipante(
                new SistemaDeConcursos(
//                        //Registro y carga en DISCO
//                        new AccesoConcursosCSV("src/main/resources/concursos.txt"),
//                        new AccesoParticipantesCSV("src/main/resources/participantes.txt")));
                        //Registro y carga en BASE DE DATOS
                        new AccesoConcursosDB(URL, USUARIO, COTRASENIA),
                        new AccesoParticipantesDB(URL, USUARIO, COTRASENIA)));

    }
}