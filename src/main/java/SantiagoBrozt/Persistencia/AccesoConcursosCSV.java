package SantiagoBrozt.Persistencia;

import SantiagoBrozt.Modelo.AccesoConcursos;
import SantiagoBrozt.Modelo.Concurso;
import SantiagoBrozt.Modelo.AccesoParticipantes;
import com.opencsv.CSVReader;

import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class AccesoConcursosCSV implements AccesoConcursos {
    private String localPath;

    public AccesoConcursosCSV(String path) {
        this.localPath = path;
    }

    @Override
    public List<Concurso> recuperarConcursos() throws RuntimeException {
        var concursos = new ArrayList<Concurso>();
        var datos = leerCSV();
        for (String[] fila : datos) {
            concursos.add(new Concurso(Integer.parseInt(fila[0]), fila[1], LocalDate.parse(fila[2]), LocalDate.parse(fila[3])));
        }
        return concursos;
    }

    private List<String[]> leerCSV() throws RuntimeException {
        List<String[]> datos = new ArrayList<String[]>();
        try {
            CSVReader reader = new CSVReader(new FileReader(localPath));
            String[] row = null;

            while ((row = reader.readNext()) != null) {
                datos.add(row);
            }
            reader.close();
            datos.remove(0);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        return datos;
    }
}
