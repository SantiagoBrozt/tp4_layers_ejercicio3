package SantiagoBrozt.Persistencia;

import SantiagoBrozt.Modelo.AccesoConcursos;
import SantiagoBrozt.Modelo.Concurso;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AccesoConcursosDB implements AccesoConcursos {
    private ConeccionDB coneccion;

    public AccesoConcursosDB(String url, String usuario, String contrasenia) throws RuntimeException {
        this.coneccion = new ConeccionDB(url, usuario, contrasenia);
    }

    @Override
    public List<Concurso> recuperarConcursos() throws RuntimeException {
        List<Concurso> listaUnidades = new ArrayList<>();
        String consulta = "SELECT * FROM concurso";
        try (Connection coneccionAbierta = this.coneccion.abrir();
             PreparedStatement st = coneccionAbierta.prepareStatement(consulta);
             ResultSet resultSet = st.executeQuery()) {
            while (resultSet.next()) {
                var concurso = new Concurso(
                        resultSet.getInt("id"),
                        resultSet.getString("nombre"),
                        resultSet.getDate("inicio_inscripcion").toLocalDate(),
                        resultSet.getDate("fin_inscripcion").toLocalDate());
                listaUnidades.add(concurso);
            }
        }
        catch (SQLException e) {
            throw new RuntimeException("Error de conexion: ", e);
        }
        return listaUnidades;
    }
}
