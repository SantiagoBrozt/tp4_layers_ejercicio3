package SantiagoBrozt.Persistencia;

import SantiagoBrozt.Modelo.AccesoParticipantes;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

    public class AccesoParticipantesDB implements AccesoParticipantes {
        private ConeccionDB coneccion;

        public AccesoParticipantesDB(String url, String usuario, String contrasenia) throws RuntimeException {
            this.coneccion = new ConeccionDB(url, usuario, contrasenia);
        }

        @Override
        public void registrarParticipante(String dni, String nombre, String apellido, String email, String phone, int concursoId) throws RuntimeException {
            String consulta = "INSERT into participante(dni, nombre, apellido, telefono, email, concurso) values(?,?,?,?,?,?)";
            try (Connection coneccionAbierta = this.coneccion.abrir();
                 PreparedStatement st = coneccionAbierta.prepareStatement(consulta)) {
                st.setInt(1, Integer.parseInt(dni));
                st.setString(2, nombre);
                st.setString(3, apellido);
                st.setString(4, phone);
                st.setString(5, email);
                st.setInt(6, concursoId);

                st.executeUpdate();
            } catch (SQLException e) {
                throw new RuntimeException("Error de conección: ", e);
            }
        }
    }
