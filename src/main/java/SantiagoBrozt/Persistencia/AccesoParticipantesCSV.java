package SantiagoBrozt.Persistencia;

import SantiagoBrozt.Modelo.AccesoParticipantes;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;


public class AccesoParticipantesCSV implements AccesoParticipantes {
    private String localPath;

    public AccesoParticipantesCSV(String path) {
        this.localPath = path;
    }

    @Override
    public void registrarParticipante(String dni, String nombre, String apellido, String phone,
                                      String email, int concursoId ) throws RuntimeException{
        String registro = apellido + "," + nombre + "," + phone + "," + email + "," + concursoId + "\n";
        try {
            Files.write(Paths.get(this.localPath),
                    registro.getBytes(),
                    StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        }
        catch (IOException e) {
            throw new RuntimeException("no se ha podido persistir", e);
        }

    }
}
