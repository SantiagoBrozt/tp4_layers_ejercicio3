package SantiagoBrozt.Modelo;

import java.util.List;

public interface SistemaDeConcursosApi {
    public List<String> nombresDeLosConcursos();
    public void inscribirParticipante(String nombre, String apellido, String dni, String phone, String email, String concurso);
}
