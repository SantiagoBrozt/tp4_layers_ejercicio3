package SantiagoBrozt.Modelo;

import java.time.LocalDate;

public class Concurso {
    private int id;
    private String nombre;
    private LocalDate finInicioInscripcion;
    private LocalDate fechaInicioInscripcion;

    public Concurso(int id, String nombre, LocalDate fechaInicioInscripcion, LocalDate finInicioInscripcion) {
        this.id = id;
        this.nombre = nombre;
        this.fechaInicioInscripcion = fechaInicioInscripcion;
        this.finInicioInscripcion = finInicioInscripcion;
    }

    public String nombre() {
        return this.nombre;
    }

    public int id() {
        return this.id;
    }
}
