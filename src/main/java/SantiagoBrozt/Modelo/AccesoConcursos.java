package SantiagoBrozt.Modelo;

import java.util.List;

public interface AccesoConcursos {
    public List<Concurso> recuperarConcursos() throws RuntimeException;
}
