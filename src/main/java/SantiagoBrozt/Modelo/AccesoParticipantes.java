package SantiagoBrozt.Modelo;

public interface AccesoParticipantes {
    public void registrarParticipante(String dni, String nombre, String apellido, String email,
                                      String phone, int concursoId ) throws RuntimeException;
}
