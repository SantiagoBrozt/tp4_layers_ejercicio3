package SantiagoBrozt.Modelo;

import javax.swing.*;

public class Participante {
    private String name;
    private String lastName;
    private String dni;
    private String phone;
    private String email;

    public Participante(String name, String lastName, String dni, String phone, String email) {
        this.validarDatos(name, lastName, dni, phone, email);
        this.name = name;
        this.lastName = lastName;
        this.dni= dni;
        this.phone = phone;
        this.email = email;
    }

    private void validarDatos(String name, String lastName, String dni, String phone, String email) throws RuntimeException {
        if (name.equals("")) {
            throw new RuntimeException("Nombre no puede ser vacio");
        }
        if (lastName.equals("")) {
            throw new RuntimeException("Apellido no puede ser vacio");
        }
        if (dni.equals("")) {
            throw new RuntimeException("Dni no puede ser vacio");
        }
        if (!checkPhone(phone)) {
            throw new RuntimeException("El teléfono debe ingresarse de la siguiente forma: NNNN-NNNNNN");
        }
        if (!checkEmail(email)) {
            throw new RuntimeException("Email debe ser válido");
        }
    }

    private boolean checkEmail(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    private boolean checkPhone(String telefono) {
        String regex = "\\d{4}-\\d{6}";
        return telefono.matches(regex);
    }
}
