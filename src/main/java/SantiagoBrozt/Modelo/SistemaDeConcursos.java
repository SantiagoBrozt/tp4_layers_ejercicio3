package SantiagoBrozt.Modelo;

import java.util.ArrayList;
import java.util.List;

public class SistemaDeConcursos implements SistemaDeConcursosApi {
    private AccesoConcursos accesoConcursos;
    private AccesoParticipantes accesoParticipantes;

    public SistemaDeConcursos(AccesoConcursos accesoConcursos, AccesoParticipantes accesoParticipantes) {
        this.accesoConcursos = accesoConcursos;
        this.accesoParticipantes = accesoParticipantes;
    }

    public List<String> nombresDeLosConcursos() {
        var concursos = this.accesoConcursos.recuperarConcursos();
        ArrayList<String> nombresConcursos = new ArrayList<>();
        for (Concurso c : concursos) {
            nombresConcursos.add(c.nombre());
        }
        return nombresConcursos;
    }

    @Override
    public void inscribirParticipante(String nombre, String apellido, String dni, String phone, String email, String concurso) throws RuntimeException{
        new Participante(nombre, apellido, dni, phone, email);
        this.accesoParticipantes.registrarParticipante(dni, nombre, apellido, phone, email, RecuperarIdConcursoPorNombre(concurso));
    }

    private int RecuperarIdConcursoPorNombre(String nombre){
        int idConcurso = 0;
        var concursos = accesoConcursos.recuperarConcursos();
        for (Concurso c : concursos) {
            if(c.nombre().equals(nombre)){
                idConcurso = c.id();
            }
        }
        return idConcurso;
    }
}

